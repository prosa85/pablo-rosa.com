import Vue from 'vue'
import Router from 'vue-router'
import KeenUI from 'keen-ui'
import BootstrapVue from 'bootstrap-vue'
import VueScrollTo from 'vue-scrollto'

// importing css
import 'keen-ui/dist/keen-ui.css'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import '@/assets/css/styles.css'

// import HelloWorld from '@/components/main'
import about from '@/components/about'
// import skills from '@/components/skills'
// import contact from '@/components/contact'
import WelcomeBanner from '@/components/banners/welcome-banner'
import SkillsBanner from '@/components/banners/skills-banner'
import ContactBanner from '@/components/banners/contact-banner'
import TutorialBanner from '@/components/banners/tutorialBanner'
const skills = () => import('@/components/skills')
const contact = () => import('@/components/contact')
const tutorials = () => import('@/components/tutorials/tutorials')
const modules = () => import('@/components/ModuleDocs/home')
const error404 = () => import('@/components/errors/404')
const banner404 = () => import('@/components/banners/error404')

Vue.use(Router)
Vue.use(KeenUI)
Vue.use(BootstrapVue)
// You can also pass in the default options
Vue.use(VueScrollTo, {
  container: 'body',
  duration: 500,
  easing: 'ease',
  offset: 0,
  cancelable: true,
  onDone: false,
  onCancel: false,
  x: false,
  y: true
})

export default new Router({
  mode: 'history',
  scrollBehavior (to, from, savedPosition) {
    return { x: 0, y: 0 }
  },
  routes: [
    {
      path: '/',
      name: 'home',
      meta: { scrollToTop: true },
      components: {
        banner: WelcomeBanner,
        first: about
      }
    },
    {
      path: '/about',
      name: 'about',
      components: {
        banner: SkillsBanner,
        second: skills
      }
    },
    // {
    //   path: '/services',
    //   name: 'services',
    //   components: {
    //     banner: HelloWorld,
    //     first: WelcomeBanner
    //   }
    // },
    // {
    //   path: '/story',
    //   name: 'story',
    //   components: {
    //     banner: HelloWorld,
    //     first: WelcomeBanner
    //   }
    // },
    // {
    //   path: '/work',
    //   name: 'work',
    //   components: {
    //     banner: HelloWorld,
    //     first: WelcomeBanner
    //   }
    // },
    // {
    //   path: '/blog',
    //   name: 'blog',
    //   components: {
    //     banner: HelloWorld,
    //     first: WelcomeBanner
    //   }
    // },
    {
      path: '/contact',
      name: 'contact',
      components: {
        banner: ContactBanner,
        first: contact
      }
    },
    {
      path: '/hubspot-modules',
      name: 'modules',
      components: {
        banner: TutorialBanner,
        first: modules
      },
      children: [
        {
          path: '',
          name: 'hubspot-home',
          component: () => import('@/components/ModuleDocs/hubspot-home')
        },
        {
          path: 'hubspot-module-google-maps',
          name: 'hubspot-module-google-maps',
          component: () => import('@/components/ModuleDocs/hubspot-module-google-maps')
        }
      ]
    },
    {
      path: '/tutorials',
      name: 'tutorials',
      components: {
        banner: TutorialBanner,
        first: tutorials
      },
      children: [
        {
          path: '',
          name: 'rootTutorials',
          component: () => import('@/components/tutorials/tutorials-body')
        },
        {
          path: 'hubspot-hbldb-and-vue-js',
          name: 'tutorials-hbl',
          component: () => import('@/components/tutorials/hubspot-hbldb-and-vue-js')
        },
        {
          path: 'google-analytics-vue-single-page-app-tracking',
          name: 'tutorials-google-a',
          component: () => import('@/components/tutorials/hubspot-hbldb-and-vue-js')
        },
        {
          path: 'hubspot-blog-listing-canonical-issues',
          name: 'canonical-blog-issue',
          component: () => import('@/components/tutorials/how-to-fix-hubspot-blog-listing-canonical-issues.vue')
        }
      ]
    },
    {
      path: '/apps',
      name: 'apps',
      components: {
        banner: () => import('@/components/banners/appsBanner'),
        first: () => import('@/components/ModuleDocs/apps.vue')
      },
      children: [
        {
          path: 'concrete-calculator',
          name: 'concrete-calculator',
          component: () => import('@/components/apps/concrete-calculator.vue')
        }
      ]
    },
    {
      path: '*',
      name: '404',
      components: {
        banner: banner404,
        first: error404
      }
    }
  ]
})
