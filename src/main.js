// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Meta from 'vue-meta'
import VueResource from 'vue-resource'
import VueCookies from 'vue-cookies'
import Vuex from 'vuex'
import VueAnalytics from 'vue-analytics'
import VueKonva from 'vue-konva'

Vue.use(VueKonva)

Vue.use(VueAnalytics, {
  id: 'UA-125800411-1'
})

Vue.config.productionTip = false

Vue.use(Vuex)
// Vue.use(autotract)
Vue.use(VueCookies)
Vue.use(Meta)
Vue.use(VueResource)
/* eslint-disable no-new */

Vue.directive('scroll', {
  inserted: function (el, binding) {
    let f = function (evt) {
      if (binding.value(evt, el)) {
        window.removeEventListener('scroll', f)
      }
    }
    window.addEventListener('scroll', f)
  }
})

const store = new Vuex.Store({
  state: {
    pagetitle: 'Tutorials'
  },
  mutations: {
    setTitle (state, title) {
      state.pagetitle = title
    },
    decrement: state => state.count--
  },
  getters: {
    getPageTitle: state => {
      return state.pagetitle
    }
  }
})

new Vue({
  el: '#app',
  store,
  router,
  template: '<App/>',
  components: { App }

})
