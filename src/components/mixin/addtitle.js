export const addtitle = {
  methods: {
    setTitle (title) {
      this.$store.commit('setTitle', title)
    },
    handleScroll: function (evt, el) {
      let b = el.getBoundingClientRect()
      // if (window.scrollY > b.top) {
      if ((b.top + 300 - window.innerHeight) < window.scrollY) {
        console.log([b.top, b.top - window.innerHeight, window.scrollY])
        el.setAttribute(
          'style',
          'opacity: 1; transform: translate3d(0, -10px, 0)'
        )
      }
    }
  },
  mounted () {
    this.setTitle(this.title)
    // this.ga('set', 'page', this.$route.fullPath)
    // this.ga('send', this.$route.fullPath)
    // this.$ga.page(this.$route.fullPath)
    // console.log(this.$route.fullPath)
  },
  destroyed () {
    this.setTitle('Tutorials')
  }

}
